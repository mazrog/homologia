---
title: Démonstration
body_classes: title-center title-h1h2

content:
  items: '@self.children'
---

{% for p in page.collection %}
  <h4><a href="{{ page.route }}/{{ p.slug }}">{{ p.title }}</a></h4>
  {{ p.summary }}
{% endfor %}
