---
title: Algèbre
body_classes: title-center title-h1h2
---

- <a href="{{ page.route }}#equality">Montrer l'égalité</a>
- <a href="{{ page.route }}#inequality">Montrer l'infériorité/la supériorité</a>

===

## {{ page.title }}

<h3 id="equality">Montrer l'égalité</h3>

- Factorisation
- Distribution
- Expression temporaire (Montrer que A = C et B = C)

<h3 id="inequality">Montrer l'inégalité</h3>

- Transitivité des comparaisons