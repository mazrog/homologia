---
title: Méthodologie
body_classes: title-center title-h1h2
slug: methodologie

content:
  items: '@self.children'
---

<h2>{{ page.title }}</h2>

<p>Cette page répertorie différentes méthodes répondant à des problèmes classiques.</p>

<ul>
{% for p in page.collection %}
  <li><a href="#{{ p.slug }}">{{ p.title }}</a></li>
{% endfor %}
</ul>

{% for p in page.collection %}
  <h3 id="{{ p.slug }}">{{ p.title }}</h3>
  <div>
    {{ p.content }}
  </div>
{% endfor %}
